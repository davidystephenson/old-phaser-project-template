import Phaser from 'phaser'

import PhaserLogo from '../objects/phaserLogo'
import FpsText from '../objects/FpsText'

export default class MainScene extends Phaser.Scene {
  fpsText: Phaser.GameObjects.Text
  logo: PhaserLogo
  x: number
  y: number

  constructor () {
    super({ key: 'MainScene' })
  }

  create (): void {
    this.logo = new PhaserLogo(
      this, this.cameras.main.width / 2, 0
    )
    this.fpsText = new FpsText(this)

    // display the Phaser.VERSION
    this.add
      .text(this.cameras.main.width - 15, 15, `Phaser v${Phaser.VERSION}`, {
        color: '#000000',
        fontSize: 24
      })
      .setOrigin(1, 0)
  }

  update (): void {
    this.fpsText.update()
  }
}
