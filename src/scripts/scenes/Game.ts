import Phaser from 'phaser'

import Box from '../objects/Box'
import Edge from '../objects/Edge'
import Position from '../objects/Position'
import Size from '../objects/Size'

export default class Game extends Phaser.Scene {
  lines: Phaser.Geom.Line[]
  graphics?: Phaser.GameObjects.Graphics

  constructor () {
    super({ key: 'gameplay' })

    this.lines = []
  }

  create (): void {
    this.setBackground(0x12ffff)

    this.graphics = this.add.graphics({
      lineStyle: {
        width: 5,
        color: 0xff0000
      }
    })

    const center = new Position({
      x: 100, y: 100
    })
    const square = new Size({
      width: 100, height: 100
    })
    const box = new Box({
      topLeft: center, size: square
    })
    console.log('box.eges test:', box.edges)

    console.log('first', box.size.halfHeight)
    console.log(
      'private', box.size._half
    )
    console.log('second', box.size.halfHeight)

    this.lines = box
      .edges
      .map(edge => edge.align())

    const centered = new Edge({
      a: new Position({ x: 800, y: 450 }),
      b: box.center
    })
    this.lines.push(centered.align())

    this
      .add
      .text(
        this.cameras.main.width - 15,
        15,
        `Phaser v${Phaser.VERSION}`,
        {
          color: '#000000',
          fontSize: 24
        }
      )
      .setOrigin(1, 0)
  }

  update (): void {
    this.lines.forEach(line => {
      this
        .graphics
        ?.strokeLineShape(line)
    })
  }

  setBackground = (
    color: number
  ): number => {
    const { main } = this.cameras

    main.setBackgroundColor(color)

    return color
  }
}
