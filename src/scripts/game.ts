import 'phaser'

import MainScene from './scenes/mainScene'
import Game from './scenes/Game'
import PreloadScene from './scenes/preloadScene'

const DEFAULT_WIDTH = 1600
const DEFAULT_HEIGHT = 900

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  backgroundColor: '#ffffff',
  scale: {
    parent: 'phaser-game',
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: DEFAULT_WIDTH,
    height: DEFAULT_HEIGHT
  },
  scene: [PreloadScene, Game, MainScene],
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
      gravity: { y: 400 }
    }
  }
}

function onLoad (): Phaser.Game {
  return new Phaser.Game(config)
}

window.addEventListener('load', onLoad)
