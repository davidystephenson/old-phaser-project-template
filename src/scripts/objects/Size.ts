interface Parameters {
  width?: number
  height?: number
}

type Key = 'height' | 'width'
type Maybe = number | undefined

export default class Size {
  _half: Size
  public height?: number
  public width?: number

  constructor (
    { width, height }: Parameters
  ) {
    this.width = width
    this.height = height
  }

  private getHalf (key: Key): Maybe {
    const value = this[key]

    if (value != null) {
      const halved = value / 2

      if (this._half != null) {
        this._half[key] = halved
      } else {
        const parameter = { [key]: halved }

        this._half = new Size(parameter)
      }

      return this._half[key]
    }
  }

  get halfWidth (): Maybe {
    return this.getHalf('width')
  }

  get halfHeight (): Maybe {
    return this.getHalf('height')
  }

  get half (): Size {
    this.getHalf('width')
    this.getHalf('height')

    return this._half
  }
}
