import Size from './Size'

interface Parameters {
  x: number
  y: number
}

type key = 'x' | 'y'

export default class Position {
  [key: string]: number

  public x: number
  public y: number

  constructor (
    { x, y }: Parameters,
    size?: Size
  ) {
    function setParameter (
      base: number, offset: number = 0
    ): number {
      return base + offset
    }

    this.x = setParameter(x, size?.width)
    this.y = setParameter(y, size?.height)
  }
}
