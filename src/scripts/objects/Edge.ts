import Phaser from 'phaser'

import Box from './Box'
import Position from './Position'

interface Parameters {
  a: Position
  b: Position
  box?: Box
}

export default class Edge {
  a: Position
  b: Position
  readonly box?: Box

  constructor (
    { a, b, box }: Parameters
  ) {
    this.a = a

    this.b = b

    this.box = box
    this.box?.edges.push(this)
  }

  public align (): Phaser.Geom.Line {
    return new Phaser.Geom.Line(
      this.a.x, this.a.y, this.b.x, this.b.y
    )
  }
}
