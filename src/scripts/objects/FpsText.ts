export default class FpsText
  extends Phaser.GameObjects.Text {
  constructor (scene: Phaser.Scene) {
    super(
      scene,
      10,
      10,
      '',
      { color: 'black', fontSize: '28px' }
    )

    scene.add.existing(this)
    this.setOrigin(0)
  }

  public update (): void {
    const { actualFps } = this
      .scene
      .game
      .loop
    const floored = Math.floor(actualFps)

    const message = `fps: ${floored}`

    this.setText(message)
  }
}
