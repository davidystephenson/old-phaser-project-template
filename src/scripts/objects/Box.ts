import Edge from './Edge'
import Position from './Position'
import Size from './Size'

interface Parameters {
  topLeft: Position
  size: Size
}

export default class Box {
  readonly bottom: Edge
  readonly bottomLeft: Position
  readonly bottomRight: Position
  readonly left: Edge
  readonly right: Edge
  readonly top: Edge
  readonly topLeft: Position
  readonly topRight: Position

  public _center: Position
  public edges: Edge[]
  public size: Size

  constructor (
    { topLeft, size }: Parameters
  ) {
    this.topLeft = topLeft
    this.size = size

    this.edges = []

    const {
      position: topRight,
      edge: top
    } = this.addCorner(topLeft, 'width')
    this.topRight = topRight
    this.top = top

    const {
      position: bottomLeft,
      edge: left
    } = this.addCorner(topLeft, 'height')
    this.bottomLeft = bottomLeft
    this.left = left

    const {
      position: bottomRight,
      edge: right
    } = this.addCorner(topRight, 'height')
    this.bottomRight = bottomRight
    this.right = right

    this.bottom = new Edge({
      a: this.bottomLeft,
      b: this.bottomRight,
      box: this
    })
  }

  addCorner (
    base: Position, key: 'width' | 'height'
  ): { position: Position, edge: Edge } {
    const value = this.size[key]
    const parameter = { [key]: value }
    const size = new Size(parameter)

    const position = new Position(
      base, size
    )

    const edge = new Edge({
      a: base, b: position, box: this
    })

    return { position, edge }
  }

  get center (): Position {
    if (this._center == null) {
      this._center = new Position(
        this.topLeft, this.size.half
      )
    }

    return this._center
  }
}
